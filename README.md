# Stan's Rubix cube demo #

Project created for a job assessment.

## Installation ##

* Open `RubixCube.sln` in visual studio 2019+.
* Nuget Restore
* Run project

## Approach ##

The Rubix cube has 6 faces, labelled as Front, Back, Left, Right, Up, Down.

The `Face` class contains the details of a single face.

The `Cube` class contains a single cube.

For the purpose of this demo only a 3x3 cube has been implemented, the rotation logic is handled by the `CubeMutator3x3` class.

Manipulation of the `Cube` within `CubeMutator3x3` uses delegate functions to link commands to the delegate to call, this would allow you to create additional mutators for larger sized cubes in future. The binding, and processing of commands is handled by a generic `CubeMutationManager`.

Commands are passed to the `CubeMutator3x3.ProcessCommands`  as a CSV list, however the underlying `CubeMutationManager` can accept them as a list of strings.

The current use of a csv is because it would be easy to implement a version which asks the user for the commands to carry out as a CSV, however this has not been implemented.

Objects and methods are treated in as close to a `functional` way as possible, this means that passed in objects are not manipulated directly.

The project splits out the `classes` and logic in to the `RubixCubeLib` project, which allows for testing to be done.

The project currently has the following tests:

* Ensuring that the demo command `F,R',U,B',L,D'` gives the expected result
* That a rotation of the front for a known setup gives the expected result
* That an attempt to call `CubeMutator3x3.ProcessCommands` using a cube of the wrong size gives an error.

These examples are limited, though it would be expected that there would be a test of each rotation clockwise and anticlockwise, along with different configurations.

This could be implemented using `TestCaseSource` to return test data and the expected result. This has not been done due to time considerations.