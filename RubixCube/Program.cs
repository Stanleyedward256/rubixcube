﻿using System;
using RubixCubeLib;

namespace RubixCube
{
    class Program
    {
        static void Main(string[] args)
        {
            Cube cube = new Cube(3, "g", "b", "o", "r", "w", "y");

            CubeMutator3x3 mutator = new CubeMutator3x3();

            var result = mutator.ProcessCommands(cube, "F,R',U,B',L,D'");
            

            if (result.Success)
            {
                Console.WriteLine(result.Cube.ToString(String.Empty, null));
            }
            else
            {
                Console.WriteLine($"Error processing cube: {result.Message}");
            }
        }
    }
}
