﻿using System;
using System.Text;

namespace RubixCubeLib
{
    public class Cube : IFormattable
    {
        public Face Front { get; set; }
        public Face Back { get; set; }
        public Face Left { get; set; }
        public Face Right { get; set; }
        public Face Up { get; set; }
        public Face Down { get; set; }

        public uint Size { get; set; }

        public Cube(uint size, string frontCol, string backCol, string leftCol, string rightCol, string upCol, string downCol)
        {
            Size = size;

            Front = new Face("Front", size);
            Front.PopulateWithSingleColour(frontCol);

            Back = new Face("Back", size);
            Back.PopulateWithSingleColour(backCol);

            Left = new Face("Left", size);
            Left.PopulateWithSingleColour(leftCol);

            Right = new Face("Right", size);
            Right.PopulateWithSingleColour(rightCol);

            Up = new Face("Up", size);
            Up.PopulateWithSingleColour(upCol);

            Down = new Face("Down", size);
            Down.PopulateWithSingleColour(downCol);
        }

        public Cube(Cube orig)
        {
            Front = new Face(orig.Front);
            Back = new Face(orig.Back);
            Left = new Face(orig.Left);
            Right = new Face(orig.Right);
            Up = new Face(orig.Up);
            Down = new Face(orig.Down);
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            StringBuilder result = new StringBuilder();

            result.Append(Front.ToString(format, formatProvider));
            result.Append(Back.ToString(format, formatProvider));
            result.Append(Left.ToString(format, formatProvider));
            result.Append(Right.ToString(format, formatProvider));
            result.Append(Up.ToString(format, formatProvider));
            result.Append(Down.ToString(format, formatProvider));

            return result.ToString();
        }
    }
}
