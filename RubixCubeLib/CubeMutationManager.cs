﻿using System;
using System.Collections.Generic;
using System.Linq;
using RubixCubeLib.ResponseModels;

namespace RubixCubeLib
{
    public class CubeMutationManager
    {
        public delegate Cube RotateCubeFaceMethod(Cube cube);

        private Dictionary<string, RotateCubeFaceMethod> _rotationMethods = new Dictionary<string, RotateCubeFaceMethod>();

        public bool AddMethod(string command, RotateCubeFaceMethod method)
        {
            if(_rotationMethods.ContainsKey(command))
            {
                return false;
            }
            _rotationMethods.Add(command, method);

            return true;
        }

        public ProcessCommandResponseModel processCommandCSV(Cube source, string csv)
        {
            List<string> commands = csv.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            return processCommandList(source, commands);
        }

        public ProcessCommandResponseModel processCommandList(Cube source, List<string> commands)
        {
            Cube resultingCube = new Cube(source);

            foreach(string command in commands)
            {
                if (!_rotationMethods.ContainsKey(command.Trim()))
                {
                    return new ProcessCommandResponseModel(null, false, $"'{command}' is not a valid command");
                }
                else
                {
                    resultingCube = _rotationMethods[command](resultingCube);
                }
            }

            return new ProcessCommandResponseModel(resultingCube, true, String.Empty);
        }

    }
}
