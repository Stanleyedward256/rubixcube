﻿using RubixCubeLib.ResponseModels;

namespace RubixCubeLib
{
    /// <summary>
    /// Handles manipulation of a 3x3 cube.
    /// </summary>
    public class CubeMutator3x3
    {
        #region Rotation delegates
        /// <summary>
        /// Rotates the squares on a face clockwise, without affecting the other faces.
        /// </summary>
        public static Face RotateFaceSquaresClockwise(Face source)
        {
            Face dest = new Face(source);

            dest.SetValue(0, 0, source.GetValue(0, 2));
            dest.SetValue(1, 0, source.GetValue(0, 1));
            dest.SetValue(2, 0, source.GetValue(0, 0));
            dest.SetValue(0, 1, source.GetValue(1, 2));
            dest.SetValue(1, 1, source.GetValue(1, 1));
            dest.SetValue(2, 1, source.GetValue(1, 0));
            dest.SetValue(0, 2, source.GetValue(2, 2));
            dest.SetValue(1, 2, source.GetValue(2, 1));
            dest.SetValue(2, 2, source.GetValue(2, 0));

            return dest;
        }

        public static CubeMutationManager.RotateCubeFaceMethod RotateFrontEdgeClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);
            
            modCube.Up.SetValue(2, 2, cube.Left.GetValue(2, 0));
            modCube.Up.SetValue(1, 2, cube.Left.GetValue(2, 1));
            modCube.Up.SetValue(0, 2, cube.Left.GetValue(2, 2));

            modCube.Right.SetValue(0, 0, cube.Up.GetValue(0, 2));
            modCube.Right.SetValue(0, 1, cube.Up.GetValue(1, 2));
            modCube.Right.SetValue(0, 2, cube.Up.GetValue(2, 2));

            modCube.Down.SetValue(0, 0, cube.Right.GetValue(0, 2));
            modCube.Down.SetValue(1, 0, cube.Right.GetValue(0, 1));
            modCube.Down.SetValue(2, 0, cube.Right.GetValue(0, 0));

            modCube.Left.SetValue(2, 0, cube.Down.GetValue(0, 0));
            modCube.Left.SetValue(2, 1, cube.Down.GetValue(1, 0));
            modCube.Left.SetValue(2, 2, cube.Down.GetValue(2, 0));

            modCube.Front = RotateFaceSquaresClockwise(cube.Front);

            return modCube;
        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateFrontEdgeAntiClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);
            for (int i = 0; i < 3; i += 1)
            {
                modCube = RotateFrontEdgeClockwise(modCube);
            }

            return modCube;
        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateBackEdgeClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);

            modCube.Down.SetValue(0, 2, cube.Left.GetValue(0, 0));
            modCube.Down.SetValue(1, 2, cube.Left.GetValue(0, 1));
            modCube.Down.SetValue(2, 2, cube.Left.GetValue(0, 2));

            modCube.Right.SetValue(2, 0, cube.Down.GetValue(2, 2));
            modCube.Right.SetValue(2, 1, cube.Down.GetValue(1, 2));
            modCube.Right.SetValue(2, 2, cube.Down.GetValue(0, 2));

            modCube.Up.SetValue(0, 0, cube.Right.GetValue(2, 0));
            modCube.Up.SetValue(1, 0, cube.Right.GetValue(2, 1));
            modCube.Up.SetValue(2, 0, cube.Right.GetValue(2, 2));

            modCube.Left.SetValue(0, 0, cube.Up.GetValue(2, 0));
            modCube.Left.SetValue(0, 1, cube.Up.GetValue(1, 0));
            modCube.Left.SetValue(0, 2, cube.Up.GetValue(0, 0));

            modCube.Back = RotateFaceSquaresClockwise(cube.Back);

            return modCube;

        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateBackEdgeAntiClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);
            for(int i = 0; i < 3; i+= 1)
            {
                modCube = RotateBackEdgeClockwise(modCube);
            }

            return modCube;
        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateLeftEdgeClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);

            modCube.Up.SetValue(0, 0, cube.Back.GetValue(2, 2));
            modCube.Up.SetValue(0, 1, cube.Back.GetValue(2, 1));
            modCube.Up.SetValue(0, 2, cube.Back.GetValue(2, 0));

            modCube.Back.SetValue(2, 2, cube.Down.GetValue(0, 0));
            modCube.Back.SetValue(2, 1, cube.Down.GetValue(0, 1));
            modCube.Back.SetValue(2, 0, cube.Down.GetValue(0, 2));

            modCube.Down.SetValue(0, 0, cube.Front.GetValue(0, 0));
            modCube.Down.SetValue(0, 1, cube.Front.GetValue(0, 1));
            modCube.Down.SetValue(0, 2, cube.Front.GetValue(0, 2));

            modCube.Front.SetValue(0, 0, cube.Up.GetValue(0, 0));
            modCube.Front.SetValue(0, 1, cube.Up.GetValue(0, 1));
            modCube.Front.SetValue(0, 2, cube.Up.GetValue(0, 2));

            modCube.Left = RotateFaceSquaresClockwise(cube.Left);

            return modCube;

        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateLeftEdgeAntiClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);
            for (int i = 0; i < 3; i += 1)
            {
                modCube = RotateLeftEdgeClockwise(modCube);
            }

            return modCube;
        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateRightEdgeClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);

            modCube.Back.SetValue(0, 2, cube.Up.GetValue(2, 0));
            modCube.Back.SetValue(0, 1, cube.Up.GetValue(2, 1));
            modCube.Back.SetValue(0, 0, cube.Up.GetValue(2, 2));

            modCube.Down.SetValue(2, 0, cube.Back.GetValue(0, 2));
            modCube.Down.SetValue(2, 1, cube.Back.GetValue(0, 1));
            modCube.Down.SetValue(2, 2, cube.Back.GetValue(0, 0));

            modCube.Front.SetValue(2, 0, cube.Down.GetValue(2, 0));
            modCube.Front.SetValue(2, 1, cube.Down.GetValue(2, 1));
            modCube.Front.SetValue(2, 2, cube.Down.GetValue(2, 2));

            modCube.Up.SetValue(2, 0, cube.Front.GetValue(2, 0));
            modCube.Up.SetValue(2, 1, cube.Front.GetValue(2, 1));
            modCube.Up.SetValue(2, 2, cube.Front.GetValue(2, 2));

            modCube.Right = RotateFaceSquaresClockwise(cube.Right);

            return modCube;
        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateRightEdgeAntiClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);
            for (int i = 0; i < 3; i += 1)
            {
                modCube = RotateRightEdgeClockwise(modCube);
            }

            return modCube;
        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateUpEdgeClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);

            modCube.Left.SetValue(0, 0, cube.Front.GetValue(0, 0));
            modCube.Left.SetValue(1, 0, cube.Front.GetValue(1,0));
            modCube.Left.SetValue(2, 0, cube.Front.GetValue(2, 0));

            modCube.Front.SetValue(0, 0, cube.Right.GetValue(0, 0));
            modCube.Front.SetValue(1, 0, cube.Right.GetValue(1, 0));
            modCube.Front.SetValue(2, 0, cube.Right.GetValue(2, 0));

            modCube.Right.SetValue(0, 0, cube.Back.GetValue(0, 0));
            modCube.Right.SetValue(1, 0, cube.Back.GetValue(1, 0));
            modCube.Right.SetValue(2, 0, cube.Back.GetValue(2, 0));

            modCube.Back.SetValue(0, 0, cube.Left.GetValue(0, 0));
            modCube.Back.SetValue(1, 0, cube.Left.GetValue(1, 0));
            modCube.Back.SetValue(2, 0, cube.Left.GetValue(2, 0));

            modCube.Up = RotateFaceSquaresClockwise(cube.Up);

            return modCube;

        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateUpEdgeAntiClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);
            for (int i = 0; i < 3; i += 1)
            {
                modCube = RotateUpEdgeClockwise(modCube);
            }

            return modCube;

        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateDownEdgeClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);

            modCube.Left.SetValue(0, 2, cube.Back.GetValue(0, 2));
            modCube.Left.SetValue(1, 2, cube.Back.GetValue(1, 2));
            modCube.Left.SetValue(2, 2, cube.Back.GetValue(2, 2));

            modCube.Front.SetValue(0, 2, cube.Left.GetValue(0, 2));
            modCube.Front.SetValue(1, 2, cube.Left.GetValue(1, 2));
            modCube.Front.SetValue(2, 2, cube.Left.GetValue(2, 2));

            modCube.Right.SetValue(0, 2, cube.Front.GetValue(0, 2));
            modCube.Right.SetValue(1, 2, cube.Front.GetValue(1, 2));
            modCube.Right.SetValue(2, 2, cube.Front.GetValue(2, 2));

            modCube.Back.SetValue(0, 2, cube.Right.GetValue(0, 2));
            modCube.Back.SetValue(1, 2, cube.Right.GetValue(1, 2));
            modCube.Back.SetValue(2, 2, cube.Right.GetValue(2, 2));

            modCube.Down = RotateFaceSquaresClockwise(cube.Down);

            return modCube;

        };

        public static CubeMutationManager.RotateCubeFaceMethod RotateDownEdgeAntiClockwise = (Cube cube) =>
        {
            Cube modCube = new Cube(cube);
            for (int i = 0; i < 3; i += 1)
            {
                modCube = RotateDownEdgeClockwise(modCube);
            }

            return modCube;


        };
        #endregion

        CubeMutationManager _cubeMutationManager;

        public CubeMutator3x3()
        {
            _cubeMutationManager = new CubeMutationManager();
            _cubeMutationManager.AddMethod("F", RotateFrontEdgeClockwise);
            _cubeMutationManager.AddMethod("F'", RotateFrontEdgeAntiClockwise);

            _cubeMutationManager.AddMethod("B", RotateBackEdgeClockwise);
            _cubeMutationManager.AddMethod("B'", RotateBackEdgeAntiClockwise);

            _cubeMutationManager.AddMethod("L", RotateLeftEdgeClockwise);
            _cubeMutationManager.AddMethod("L'", RotateLeftEdgeAntiClockwise);

            _cubeMutationManager.AddMethod("R", RotateRightEdgeClockwise);
            _cubeMutationManager.AddMethod("R'", RotateRightEdgeAntiClockwise);

            _cubeMutationManager.AddMethod("U", RotateUpEdgeClockwise);
            _cubeMutationManager.AddMethod("U'", RotateUpEdgeAntiClockwise);

            _cubeMutationManager.AddMethod("D", RotateDownEdgeClockwise);
            _cubeMutationManager.AddMethod("D'", RotateDownEdgeAntiClockwise);
        }

        public ProcessCommandResponseModel ProcessCommands(Cube cube, string commands)
        {
            if(cube.Size != 3)
            {
                return new ProcessCommandResponseModel(cube, false, "The CubeMutator3x3 only works for cubes with a size of 3.");
            }
            return _cubeMutationManager.processCommandCSV(cube, commands);
        }

    }
}
