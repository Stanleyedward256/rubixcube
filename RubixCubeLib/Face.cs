﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RubixCubeLib
{
    public class Face : IFormattable
    {
        public uint Size;

        public List<string> Squares = new List<string>();

        public string Name;

        public Face(string name, uint size)
        {
            Name = name;
            Size = size;
        }

        public Face(Face orig)
        {
            Name = orig.Name;
            Size = orig.Size;
            Squares = new List<string>(orig.Squares);

        }

        public void PopulateWithSingleColour(string color)
        {
            Squares = Enumerable.Repeat<string>(color, (int)(Size * Size)).ToList();
        }

        public bool PopulateFromArray(string[,] elements)
        {
            PopulateWithSingleColour("");

            int rowCount = elements.GetLength(0);
            int columnCount = elements.GetLength(1);
            if(rowCount != Size || columnCount != Size)
            {
                return false;
            }
            else
            {
                for(uint y = 0; y < Size; y += 1)
                {
                    for (uint x = 0; x < Size; x += 1)
                    {
                        SetValue(y, x, elements[x, y]);
                    }
                }

                return true;
            }
        }

        public void SetValue(uint x, uint y, string color)
        {
            if (x < Size && y < Size)
            {
                uint index = (y * Size) + x;

                Squares[(int)index] = color;
            }
        }

        public string GetValue(uint x, uint y)
        {
            if (x >= Size || y >= Size)
            {
                return String.Empty;
            }

            uint index = (y * Size) + x;

            return Squares[(int)index];
        }

        public string ToString(string format, IFormatProvider formatProvider)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine(String.Empty);
            result.AppendLine(Name);
            result.AppendLine(new string('=', Name.Length));

            for(int i = 0; i < Size * Size; i += 1)
            {
                result.Append(Squares[i]);
                if(i % Size == (Size - 1))
                {
                    result.Append("\n");
                }
                else
                {
                    result.Append(", ");
                }
            }

            return result.ToString();
        }
    }
}
