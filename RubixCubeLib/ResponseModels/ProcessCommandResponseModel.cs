﻿namespace RubixCubeLib.ResponseModels
{
    public class ProcessCommandResponseModel
    {
        public Cube Cube { get; private set; }
        public bool Success { get; private set; }

        public string Message { get; private set; }

        public ProcessCommandResponseModel(Cube cube, bool success, string message = "")
        {
            Cube = cube;
            Success = success;
            Message = message;
        }
    }
}
