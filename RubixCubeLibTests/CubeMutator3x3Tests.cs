using NUnit.Framework;
using RubixCubeLib;
using RubixCubeLib.ResponseModels;
using FluentAssertions;

namespace RubixCubeLibTests
{
    public class CubeMutator3x3Tests
    {
        /// <summary>
        /// Validates the setup required for this example app.
        /// </summary>
        [Test]
        public void PuzzleIsSolvedForDemoInput()
        {
            // Arrange
            Cube cube = new Cube(3, "g", "b", "o", "r", "w", "y");

            CubeMutator3x3 cubeMutator = new CubeMutator3x3();

            Cube expectedCube = new Cube(3, "g", "b", "o", "r", "w", "y");
            string[,] expectedLeftArray = { { "g", "y", "y" }, { "o", "o", "g" }, { "b", "g", "o" } };
            string[,] expectedUpArray = { { "r", "o", "g" }, { "b", "w", "w" }, { "b", "b", "b" } };
            string[,] expectedRightArray = { { "y", "b", "o" }, { "r", "r", "w" }, { "o", "y", "r" } };
            string[,] expectedDownArray = { { "g", "g", "b" }, { "r", "y", "r" }, { "r", "g", "g" } };
            string[,] expectedFrontArray = { { "o", "r", "r" }, { "o", "g", "w" }, { "w", "w", "w" } };
            string[,] expectedBackArray = { { "y", "b", "w" }, { "o", "b", "y" }, { "y", "y", "w" } };

            expectedCube.Left.PopulateFromArray(expectedLeftArray);
            expectedCube.Up.PopulateFromArray(expectedUpArray);
            expectedCube.Down.PopulateFromArray(expectedDownArray);
            expectedCube.Right.PopulateFromArray(expectedRightArray);
            expectedCube.Front.PopulateFromArray(expectedFrontArray);
            expectedCube.Back.PopulateFromArray(expectedBackArray);

            // Act
            ProcessCommandResponseModel response = cubeMutator.ProcessCommands(cube, "F,R',U,B',L,D'");

            // Assert
            response.Success.Should().BeTrue();
            response.Cube.Left.Should().BeEquivalentTo(expectedCube.Left);
            response.Cube.Right.Should().BeEquivalentTo(expectedCube.Right);
            response.Cube.Up.Should().BeEquivalentTo(expectedCube.Up);
            response.Cube.Down.Should().BeEquivalentTo(expectedCube.Down);
            response.Cube.Front.Should().BeEquivalentTo(expectedCube.Front);
            response.Cube.Back.Should().BeEquivalentTo(expectedCube.Back);

        }

        /// <summary>
        /// Example of checking the rotation logic for a single face.
        /// More tests could be done by using a test group with the expected value of the faces passed in as parameters
        /// </summary>
        [Test]
        public void RotateFrontGivesExpectedResult()
        {
            // Arrange
            Cube cube = new Cube(3, "g", "b", "o", "r", "w", "y");

            CubeMutator3x3 cubeMutator = new CubeMutator3x3();

            Cube expectedCube = new Cube(3, "g", "b", "o", "r", "w", "y");
            string[,] expectedLeftArray = { { "o", "o", "y" }, { "o", "o", "y" }, { "o", "o", "y" } };
            string[,] expectedUpArray = { { "w", "w", "w" }, { "w", "w", "w" }, { "o", "o", "o" } };
            string[,] expectedRightArray = { { "w", "r", "r" }, { "w", "r", "r" }, { "w", "r", "r" } };
            string[,] expectedDownArray = { { "r", "r", "r" }, { "y", "y", "y" }, { "y", "y", "y" } };
            string[,] expectedFrontArray = { { "g", "g", "g" }, { "g", "g", "g" }, { "g", "g", "g" } };
            string[,] expectedBackArray = { { "b", "b", "b" }, { "b", "b", "b" }, { "b", "b", "b" } };

            expectedCube.Left.PopulateFromArray(expectedLeftArray);
            expectedCube.Up.PopulateFromArray(expectedUpArray);
            expectedCube.Down.PopulateFromArray(expectedDownArray);
            expectedCube.Right.PopulateFromArray(expectedRightArray);
            expectedCube.Front.PopulateFromArray(expectedFrontArray);
            expectedCube.Back.PopulateFromArray(expectedBackArray);

            // Act
            ProcessCommandResponseModel response = cubeMutator.ProcessCommands(cube, "F");

            // Assert
            response.Success.Should().BeTrue();
            response.Cube.Left.Should().BeEquivalentTo(expectedCube.Left);
            response.Cube.Right.Should().BeEquivalentTo(expectedCube.Right);
            response.Cube.Up.Should().BeEquivalentTo(expectedCube.Up);
            response.Cube.Down.Should().BeEquivalentTo(expectedCube.Down);
            response.Cube.Front.Should().BeEquivalentTo(expectedCube.Front);
            response.Cube.Back.Should().BeEquivalentTo(expectedCube.Back);
        }

        /// <summary>
        /// A test of a validation message from the code
        /// </summary>
        [Test]
        public void CubeMutator3x3DoesntAcceptCubesNotSizedAs3()
        {
            // Arrange
            Cube cube = new Cube(4, "g", "b", "o", "r", "w", "y");

            CubeMutator3x3 cubeMutator = new CubeMutator3x3();

            // Act
            ProcessCommandResponseModel response = cubeMutator.ProcessCommands(cube, "F");

            // Assert

            response.Success.Should().BeFalse();
            response.Message.Should().Be("The CubeMutator3x3 only works for cubes with a size of 3.");

        }

    }
}